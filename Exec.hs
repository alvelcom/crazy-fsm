{-# LANGUAGE OverloadedStrings, 
             NoMonomorphismRestriction, 
             TypeSynonymInstances,
             FlexibleInstances #-}

module Exec where

import Types
import Data.Maybe
import Text.Blaze
import Text.Blaze.Html5 hiding (head, map)
import Text.Blaze.Html5.Attributes hiding (step)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Pretty

import Debug.Trace

step :: Rules -> Int -> Input -> (Int, ConcreteOperation)
step rules state input = 
    if [] /= matched 
    then head matched
    else (state, OpError "Unhandled input")
  where
    matched = mapMaybe mapper rules
    tokenMatcher pos matcher = matchToken (input !! pos) matcher

    mapper (Rule s _ _ _) | s /= state 
      = Nothing
    mapper (Rule _ t (OneVar v) op)
      | length input == 1 = case tokenMatcher 0 v of
                              (False, _) -> Nothing
                              (True, Just int) -> Just (t, setConcrete int op)
                              (True, Nothing)  -> Just (t, setConcrete 0 op)
      | otherwise         = Nothing
    mapper r@(Rule _ t (TwoVars v1 v2) op) 
      | length input == 2 = case (tokenMatcher 0 v1, tokenMatcher 1 v2)  of
                              ((False, _), _) -> Nothing
                              (_, (False, _)) -> Nothing
                              ((True, Just int), (True, Nothing)) -> 
                                  Just (t, setConcrete int op)
                              ((True, Nothing), (True, Just int)) -> 
                                  Just (t, setConcrete int op)
                              ((True, Nothing), (True, Nothing)) ->
                                  Just (t, setConcrete 0 op)
                              v ->
                                  error ("Bad rule: " ++ show r ++ "; " ++ show v)
      | otherwise         = Nothing
    mapper (Rule _ t Otherwise op)
      = Just (t, setConcrete 0 op)


exec :: Machine -> ConcreteOperation -> (Machine, Maybe Html)
exec m@(Machine state lastState currentTape _tapePos tapes _rules) op
  = case op of
      OpError s ->
        (m, Just $ toMarkup ("Error: " ++ s))
      OpWarning s ->
        (m, Just $ toMarkup ("Warning: " ++ s))
      Nop ->
        (m, Nothing)
      Set tapeNum pos value ->
        let tape = setNth (tapes !! tapeNum) pos (Int_ value)
        in (m{_tapes = setNth tapes tapeNum tape}, Nothing)
      CompareAndSet tapeNum pos expected value ->
        let tape = setNth (tapes !! tapeNum) pos (Int_ value)
            m' = m{_tapes = setNth tapes tapeNum tape}
        in if tapes !! tapeNum !! pos == Int_ expected
           then (m', Nothing) 
           else (m,  Just $ toMarkup $ unwords ["Error: tape"
                             ,"#" ++ show tapeNum
                             ,": expected"
                             ,show expected
                             ,"but"
                             ,show (tapes !! tapeNum !! pos)
                             ,"found"])
      SetActiveTape tape Nothing ->
        (m{_currentTape = [tape], _tapePos = [0]}, Nothing)
      SetActiveTape tape1 (Just tape2) ->
        (m{_currentTape = [tape1, tape2], _tapePos = [0, 0]}, Nothing)

setNth (a:as) 0 value = value : as
setNth (a:as) n value | n > 0 = a : setNth as (n-1) value

getNth (a:_) 0 = a
getNth [] _ = End
getNth (_:as) n | n > 0 = getNth as (n-1)

prepareInput tapes selected poses = zipWith getNth (map (tapes !!) selected) poses

stepExec :: Machine -> (TraceLine, Machine)
stepExec m@(Machine state lastState currentTape poses tapes rules)
  = let input = prepareInput tapes currentTape poses
        poses' = (+) 1 `map` poses
        (state', op) = step rules state input
        (m', html) = exec m{_tapePos = poses'} op
        line = TraceLine 
                { _from_state = state
                , _to_state   = state'
                , _active     = currentTape
                , _input      = input
                , _position   = poses
                , _op         = op
                , _message    = html
                , _tl_tapes   = tapes
                }
    in (line, m'{_state = state', _lastState = state})

