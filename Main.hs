{-# LANGUAGE OverloadedStrings #-}

import Data.Maybe
import Types
import Report
import Text.Blaze.Html5 hiding (map)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Exec

rules :: Rules
rules =
  [ Rule 0 1 (OneVar (Action (Exactly 0))) Nop
  , Rule 0 1 Otherwise (OpError "Expected A0")
  , Rule 1 2 (OneVar (Cond Variable)) Nop
  , Rule 1 2 (OneVar Always) Nop
  , Rule 2 1 (OneVar (Goto Variable)) (Set 1 Variable (Exactly 1))
  , Rule 1 1 (OneVar (Label Variable)) (CompareAndSet 2 Variable (Exactly 0) (Exactly 1))
  , Rule 1 1 (OneVar (Action Variable)) Nop
  , Rule 1 3 (OneVar End) (SetActiveTape 1 (Just 2))
  , Rule 1 1 Otherwise (OpError "Unexpected symbol")
  , Rule 3 3 (TwoVars (Int_ (Exactly 0)) (Int_ (Exactly 0))) Nop
  , Rule 3 3 (TwoVars (Int_ (Exactly 1)) (Int_ (Exactly 1))) Nop
  , Rule 3 3 (TwoVars (Int_ (Exactly 0)) (Int_ (Exactly 1))) (OpWarning "Unused label")
  , Rule 3 3 (TwoVars (Int_ (Exactly 1)) (Int_ (Exactly 0))) (OpError   "Bad reference")
  , Rule 3 4 Otherwise Nop
  ]

run :: Machine -> [TraceLine]
run m = do
  let (line, m') = stepExec m
  if _state m' == maximum (map _to $ _rules m')
  then []
  else line : run m'

main = do
  line <- getLine
  let tokens = read line :: Tokens
  let nulls = replicate (length tokens) (Int_ 0)
      m = Machine 0 0 [0] [0] [tokens, nulls, nulls] rules
  let lines = run m
  putStrLn (show (length lines) ++ " steps")
  let pretty = prettify $ docTypeHtml $ body $ 
        do
          h2 "Input"
          toMarkup tokens
          h2 "Output messages"
          ul $ sequence_ $ map li $ mapMaybe errorPrinter lines
          h2 "Trace"
          mapM_ toMarkup lines
        where
          errorPrinter t@(TraceLine{_message=Just msg, _input=input, _position=pos}) = Just $ do
            msg
            "[at position"
            toMarkup pos
            "(input = "
            toMarkup input
            ")]"
          errorPrinter _ = Nothing



  writeFile "index.html" pretty
  return ()
    
-- vim: ts=2 sw=2 et
