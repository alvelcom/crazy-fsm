{-# LANGUAGE OverloadedStrings, 
             NoMonomorphismRestriction, 
             TypeSynonymInstances,
             FlexibleInstances #-}

module Report where

import Control.Monad (forM_, mapM_)
import Data.Maybe

import Text.Blaze
import Text.Blaze.Html5 hiding (map)
import Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Pretty

import Types

upHtml, downHtml, omegaHtml, nbsp, emptyHtml :: Html
upHtml    = preEscapedToMarkup ("&uarr;" :: String)
downHtml  = preEscapedToMarkup ("&darr;" :: String)
omegaHtml = preEscapedToMarkup ("&omega;" :: String)
emptyHtml = preEscapedToMarkup ("&empty;" :: String)
nbsp      = preEscapedToMarkup ("&nbsp;" :: String)

instance ToMarkup Token where
  toMarkup (Action int) = H.span ("A"      >> sub (toMarkup int))
  toMarkup (Goto   int) = H.span (upHtml   >> sub (toMarkup int))
  toMarkup (Label  int) = H.span (downHtml >> sub (toMarkup int))
  toMarkup (Cond   int) = H.span ("P"      >> sub (toMarkup int))
  toMarkup (Int_   int) = H.span $ toMarkup int
  toMarkup End          = H.span emptyHtml
  toMarkup Always       = H.span omegaHtml

instance ToMarkup Tokens where
  toMarkup list = mapM_ (\a -> toMarkup a >> nbsp) list 

instance ToMarkup [Int] where
  toMarkup = sequence_ . map toMarkup

htmlize = toMarkup
prettify a = renderHtml a

row :: [Html] -> Html
row items = tr $ sequence_ $ map (td' "30px") items

tapeTable :: [Tokens] -> Html
tapeTable tapes = table $ do
  row $ map toMarkup [0..length (tapes !! 0) - 1]
  mapM_ (row . map toMarkup) tapes

instance ToMarkup TraceLine where
  toMarkup (TraceLine from to active input pos op msg tapes) = do
    h4 (toMarkup from >> " -> " >> toMarkup to)
    table $ tr $ do
      td' "300px" $ do
        p  ("Input: " >> toMarkup input)
        p  ("Active tapes: " >> toMarkup active)
        p  ("Position: " >> toMarkup pos)
        p  ("Operation: " >> toMarkup (show op))
        case msg of
          Just text -> p ("Message: " >> text)
          Nothing -> ""
      td $ do
        "Tapes state:"
        tapeTable tapes

td' :: String -> Html -> Html
td' sz html = td ! A.style (toValue $ "width:" ++ sz) $ html
   
-- vim: ts=2 sw=2 et
