{-# LANGUAGE OverloadedStrings, 
             NoMonomorphismRestriction, 
             TypeSynonymInstances,
             FlexibleInstances #-}

module Types where

import Data.Char
import Text.Blaze.Html5
import Text.Blaze.Html.Renderer.String

data Token_ a = Action a
              | Goto a
              | Label a
              | Cond a
              | Always
              | Int_ a
              | LexError String
              | End
              deriving (Show, Eq)

type Token = Token_ Int
type Tokens = [Token]

data Value = Exactly Int 
           | Variable
           deriving (Show, Eq)

data Operation_ a = OpError String
                  | OpWarning String
                  | Nop
                  | Set Int a a
                  | CompareAndSet Int a a a
                  | SetActiveTape Int (Maybe Int)
                  deriving (Show, Eq)

type Operation = Operation_ Value
type ConcreteOperation = Operation_ Int

type Matcher = Token_ Value

type Input = [Token]

data Pattern = OneVar Matcher
             | TwoVars Matcher Matcher
             | Otherwise
             deriving (Show, Eq)

data Rule = Rule
  { _from    :: Int
  , _to      :: Int
  , _pattern :: Pattern
  , _operation:: Operation
  } deriving Show

type Rules = [Rule]

data Machine = Machine
  { _state       :: Int
  , _lastState   :: Int
  , _currentTape :: [Int]
  , _tapePos     :: [Int]
  , _tapes       :: [Tokens]
  , _rules       :: Rules
  } deriving Show

data TraceLine = TraceLine
  { _from_state  :: Int
  , _to_state    :: Int
  , _active      :: [Int]
  , _input       :: Tokens
  , _position    :: [Int]
  , _op          :: ConcreteOperation
  , _message     :: Maybe Html
  , _tl_tapes    :: [Tokens]
  } deriving Show
---- cut here

instance Read Token where
  readList string = [(readTokens string, "")]
    where
      readTokens [] = []
      readTokens (a:as) 
        | isSpace a   = readTokens as
        | lowA == 'a' = Action int : readTokens rest
        | lowA == 'g' = Goto   int : readTokens rest 
        | lowA == 'l' = Label  int : readTokens rest 
        | lowA == 'p' = Cond   int : readTokens rest 
        | lowA == 'o' = Always     : readTokens as
        | otherwise = LexError("Bad character '" ++ a:"'") : readTokens as
        where
          lowA = toLower a
          number = takeWhile isNumber as
          rest   = dropWhile isNumber as
          int    = read number

instance Show Html where
  show = show . renderHtml

matchToken :: Token -> Matcher -> (Bool, Maybe Int)
matchToken (Action t) (Action m) = matchVariable t m
matchToken (Cond t)   (Cond m)   = matchVariable t m
matchToken (Goto t)   (Goto m)   = matchVariable t m
matchToken (Label t)  (Label m)  = matchVariable t m
matchToken (Int_ t)   (Int_ m)   = matchVariable t m
matchToken Always Always = (True, Nothing)
matchToken End    End    = (True, Nothing)
matchToken _      _      = (False, Nothing)

matchVariable :: Int -> Value -> (Bool, Maybe Int)
matchVariable int (Exactly int_) = (int == int_, Nothing)
matchVariable int Variable = (True, Just int)
matchVariable _ _ = (False, Nothing)

setConcrete :: Int -> Operation -> ConcreteOperation
setConcrete int (Set a Variable (Exactly b)) = Set a int b
setConcrete int (Set a (Exactly b) Variable) = Set a b int
setConcrete _ (Set a (Exactly b) (Exactly c)) = Set a b c
setConcrete int (CompareAndSet a Variable (Exactly b) (Exactly c)) = CompareAndSet a int b c
setConcrete int (CompareAndSet a (Exactly b) Variable (Exactly c)) = CompareAndSet a b int c
setConcrete int (CompareAndSet a (Exactly b) (Exactly c) Variable) = CompareAndSet a b c int
setConcrete _ (CompareAndSet a (Exactly b) (Exactly c) (Exactly d)) = CompareAndSet a b c d
setConcrete _ (OpError s) = OpError s
setConcrete _ (OpWarning s) = OpWarning s
setConcrete _ Nop = Nop
setConcrete _ (SetActiveTape a b) = SetActiveTape a b

-- vim: ts=2 sw=2 et
